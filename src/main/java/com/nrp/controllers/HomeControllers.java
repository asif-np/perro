package com.nrp.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HomeControllers implements Initializable {

    String result = "";

    @FXML
    private TextArea textArea;

    @FXML
    private Button submitButton;

    public void handleSubmitButtonAction (ActionEvent event) throws IOException {


        if(textArea.getText().isEmpty()) {
            result = "OK nevermind i can read your mind!";
        }


        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/NextView.fxml"));
        Parent resultParent = loader.load();

        ResultControllers resultControllers = loader.getController();
        resultControllers.setResult(result+"\njust do it");

        Scene resultScene = new Scene(resultParent, 800, 500);



        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(resultScene);
        window.show();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}

