package com.nrp.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class ResultControllers implements Initializable {

    @FXML
    private Label result;

    public void setResult(String s) {
        result.setText(s);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
